#  **RestFood**
### Web Application for restaurants


---   

Packages Required

* [django](https://github.com/django/django)
* [djang-bower](https://github.com/nvbn/django-bower)
* [django-material](https://github.com/viewflow/django-material)
* [django-simple-captcha](https://github.com/mbi/django-simple-captcha)
* [django-newsletter](https://github.com/dokterbob/django-newsletter)
* [Pillow](https://github.com/python-pillow/Pillow)

Install packages

```python
python setup.py install

```
---   

Components Requires


* [jquery](https://github.com/jquery/jquery)
* [materialize](https://github.com/Dogfalo/materialize)

Install bower components

```python
manage.py bower install

```
---   

Admin account in **admin.json**

Fuck C