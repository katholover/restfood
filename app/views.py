#from django.shortcuts import render, render_to_response

from django.shortcuts import render, render_to_response


def index(request):
    return render(request, 'index.html', {})

def explore(request):
    return render(request, 'explore.html', {})

def home(request):
    return render(request, 'home.html', {})

def restaurant_profile(request):
    return render(request, 'restaurant_profile.html', {})

def user_profile(request):
    return render(request, 'user_profile.html', {})

def email_confirm(request):
    return render(request, 'email_confirm.html', {})

def about(request):
    return render(request, 'about.html', {})
