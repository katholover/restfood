$('.owl-carousel').owlCarousel({
    items:2,
    loop:true,
    margin:8,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    stagePadding: 30,
    autoWidth: true
});
