from django.conf.urls import url
from django.conf import settings
from django.views.static import serve
from . import views

urlpatterns = [
  url(r'^$', views.index, name="index"),
  url(r'^explore/$', views.explore, name="explore"),
  url(r'^home/$', views.home, name="home"),
  url(r'^restaurant_profile/$', views.restaurant_profile, name="restaurant_profile"),
  url(r'^user_profile/$', views.user_profile, name="user_profile"),
  url(r'^email_confirm/$', views.email_confirm, name="email_confirm"),
  url(r'^about/$', views.about, name="about")
]
